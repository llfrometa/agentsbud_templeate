(function($){
  $(function(){

      $('.button-collapse').sideNav();

      $('.collection .collection-item-new').click( function () {
          $('.from-add-group').removeClass('hiddendiv');
          $(this).addClass('hide');
      });

      $('.contact-card .collection.collection-group .from-add-group .column-btn .btn-cancel').click( function () {
          $('.from-add-group').addClass('hiddendiv');
          $('.collection .collection-item-new').removeClass('hide');
      });


  }); // end of document ready
})(jQuery); // end of jQuery name space